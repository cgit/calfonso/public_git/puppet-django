class django::server {
    package { ["httpd", "Django", "mod_python"]:
        ensure      => installed,
    }

    service { "httpd":
        ensure      => running,
        require     => Package["httpd"],
    }

    # If 'service httpd start' and 'service httpd restart' are run to closely
    # together the restart will fail.  This is a workaround.
    exec { "graceful-apache":
        command         => "/sbin/service httpd graceful",
        refreshonly     => true,
        require         => Package["httpd"],
    }

    file { "/etc/httpd/conf.d/django.conf":
	ensure	=> present,
	owner	=> "root",
	group	=> "root",
	mode	=> 0644,
	source	=> "puppet:///django/django.conf",
	require => [Package["httpd"],Package["mod_python"], Exec["Checkout django example"]],
	notify	=> Exec["graceful-apache"],
    }

    exec { "Checkout django example":
        command     => "/usr/bin/git clone git://${repoappliance}/pub/git/code/genome_django /usr/local/genome_django",
        creates     => "/usr/local/genome_django",
    }
}
